﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApi.Models
{
    public class Note
    {
        protected byte type;
        public byte Type { get { return type; } }

        public int id { get; set; }//код
        public string name { get; set; }
        public int quantity { get; set; }//количество
        public int year { get; set; }
        public string publishing { get; set; }//издательство
      
    }

}
